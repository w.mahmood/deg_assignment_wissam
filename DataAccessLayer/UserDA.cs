﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BusinessObject;

namespace DataAccessLayer
{
    public class UserDA
    {
        public IEnumerable<User> AllUsers
        {
            get
            {
                string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                List<User> Users = new List<User>();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("GetAllUsers", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = (SqlDataReader)cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        User user = new User();
                        user.UserId = Convert.ToInt32(rdr["UserId"]);
                        user.Username = rdr["Username"].ToString();
                        user.Email = rdr["Email"].ToString();
                        user.PasswordQuestion = rdr["PasswordQuestion"].ToString();
                        user.PasswordAnswer = rdr["PasswordAnswer"].ToString();
                        //user.IsApproved = rdr["IsApproved"];
                        user.RoleId = Convert.ToInt32(rdr["RoleId"]);
                        user.Language = rdr["Language"].ToString();
                        Users.Add(user);

                    }

                }
                return Users;
            }

        }

        public void addUser(User user)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            //List<User> Users = new List<User>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SaveUser", con);
                cmd.CommandType = CommandType.StoredProcedure;



                SqlParameter paramUserId = new SqlParameter();
                paramUserId.ParameterName = "@UserId";
                paramUserId.Value = user.UserId;
                cmd.Parameters.Add(paramUserId);

                SqlParameter paramUsername = new SqlParameter();
                paramUsername.ParameterName = "@Username";
                paramUsername.Value = user.Username;
                cmd.Parameters.Add(paramUsername);

                SqlParameter paramPassword = new SqlParameter();
                paramPassword.ParameterName = "@Password";
                paramPassword.Value = user.Password;
                cmd.Parameters.Add(paramPassword);

                SqlParameter paramEmail = new SqlParameter();
                paramEmail.ParameterName = "@Email";
                paramEmail.Value = user.Email;
                cmd.Parameters.Add(paramEmail);

                SqlParameter paramPasswordQuestion = new SqlParameter();
                paramPasswordQuestion.ParameterName = "@PasswordQuestion";
                paramPasswordQuestion.Value = user.PasswordQuestion.ToString();
                cmd.Parameters.Add(paramPasswordQuestion);


                SqlParameter paramPasswordAnswer = new SqlParameter();
                paramPasswordAnswer.ParameterName = "@PasswordAnswer";
                paramPasswordAnswer.Value = user.PasswordAnswer.ToString();
                cmd.Parameters.Add(paramPasswordAnswer);


                SqlParameter paramRoleId = new SqlParameter();
                paramRoleId.ParameterName = "@RoleId";
                paramRoleId.Value = user.RoleId;
                cmd.Parameters.Add(paramRoleId);

                SqlParameter paramLanguage = new SqlParameter();
                paramLanguage.ParameterName = "@Language";
                paramLanguage.Value = user.Language;
                cmd.Parameters.Add(paramLanguage);

                SqlParameter paramIsApproved = new SqlParameter();
                paramIsApproved.ParameterName = "@IsApproved";
                paramIsApproved.Value = user.IsApproved;
                cmd.Parameters.Add(paramIsApproved);

                SqlParameter paramErrN = new SqlParameter();
                paramErrN.ParameterName = "@ErrN";
                paramErrN.Value = 0;
                cmd.Parameters.Add(paramErrN);

                SqlParameter paramErrM = new SqlParameter();
                paramErrM.ParameterName = "@ErrM";
                paramErrM.Value = "";
                cmd.Parameters.Add(paramErrM);





                con.Open();
                cmd.ExecuteNonQuery();

            }

        }
        public void saveUser(User user)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            //List<User> Users = new List<User>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SaveUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramUserId = new SqlParameter();
                paramUserId.ParameterName = "@UserId";
                paramUserId.Value = user.UserId;
                cmd.Parameters.Add(paramUserId);


                SqlParameter paramUsername = new SqlParameter();
                paramUsername.ParameterName = "@Username";
                paramUsername.Value = user.Username;
                cmd.Parameters.Add(paramUsername);

                SqlParameter paramPassword = new SqlParameter();
                paramPassword.ParameterName = "@Password";
                paramPassword.Value = user.Password;
                cmd.Parameters.Add(paramPassword);

                SqlParameter paramEmail = new SqlParameter();
                paramEmail.ParameterName = "@Email";
                paramEmail.Value = user.Email;
                cmd.Parameters.Add(paramEmail);

                SqlParameter paramPasswordQuestion = new SqlParameter();
                paramPasswordQuestion.ParameterName = "@PasswordQuestion";
                paramPasswordQuestion.Value = user.PasswordQuestion;
                cmd.Parameters.Add(paramPasswordQuestion);


                SqlParameter paramPasswordAnswer = new SqlParameter();
                paramPasswordAnswer.ParameterName = "@PasswordAnswer";
                paramPasswordAnswer.Value = user.PasswordAnswer;
                cmd.Parameters.Add(paramPasswordAnswer);


                SqlParameter paramRoleId = new SqlParameter();
                paramRoleId.ParameterName = "@RoleId";
                paramRoleId.Value = user.RoleId;
                cmd.Parameters.Add(paramRoleId);

                SqlParameter paramLanguage = new SqlParameter();
                paramLanguage.ParameterName = "@Language";
                paramLanguage.Value = user.Language;
                cmd.Parameters.Add(paramLanguage);


                SqlParameter paramIsApproved = new SqlParameter();
                paramIsApproved.ParameterName = "@IsApproved";
                paramIsApproved.Value = user.IsApproved;
                cmd.Parameters.Add(paramIsApproved);

                SqlParameter paramErrN = new SqlParameter();
                paramErrN.ParameterName = "@ErrN";
                paramErrN.Value = 0;
                cmd.Parameters.Add(paramErrN);

                SqlParameter paramErrM = new SqlParameter();
                paramErrM.ParameterName = "@ErrM";
                paramErrM.Value = "";
                cmd.Parameters.Add(paramErrM);



                con.Open();
                cmd.ExecuteNonQuery();

            }
        }


        public void deleteUser(int id)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("DeleteUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramId = new SqlParameter();
                paramId.ParameterName = "@UserID";
                paramId.Value = id;
                cmd.Parameters.Add(paramId);


                con.Open();
                cmd.ExecuteNonQuery();

            }
        }


    }
}

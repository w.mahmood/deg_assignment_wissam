﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using BusinessLayer;
using BusinessObject;

namespace WebApplication1.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            UserBusinessLayer UserBusinessLayer = new UserBusinessLayer();
            List<User> Users = UserBusinessLayer.Users.ToList();
            return View(Users);
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {

            return View();
        }

        [HttpPost]
        [ActionName("Create")]
        public ActionResult Create_Post(User user)
        {
            /*User emp = new User();
             TryUpdateModel<User>(emp);//updateModel throws an exception if validation fails but TryUpdateModel will never throw an exception
            */
            if (ModelState.IsValid)
            {

                UserBusinessLayer userBL = new UserBusinessLayer();
                userBL.addUser(user);
                return RedirectToAction("Index");
            }

            return View();
        }


        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(int id)
        {
            UserBusinessLayer userBL = new UserBusinessLayer();
            User User = userBL.Users.Single(user => user.UserId == id);
            return View(User);
        }


        [HttpPost]
        [ActionName("Edit")]
        public ActionResult Edit_Post(int id)
        {

            UserBusinessLayer userBL = new UserBusinessLayer();
            User User = userBL.Users.Single(user => user.UserId == id);
            UpdateModel<User>(User);//include property
            //UpdateModel<User>(User, new string []{"Id","Gender","City","DateOfBirth"});// Exclude properties
            //or you can use bind attribute or using interface to include and exclude properties
            if (ModelState.IsValid)
            {
                userBL.saveUser(User);
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            UserBusinessLayer empBL = new UserBusinessLayer();
            empBL.deleteUser(id);
            return RedirectToAction("Index");
        }


        public ActionResult ChangeCultureEN()
        {
            Session["CurrentLanguage"] = "en-US";
            Class1.lang = "en-US";
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult ChangeCultureAR()
        {
            Session["CurrentLanguage"] = "ar-SY";
            Class1.lang = "ar-SY";
            return Redirect(Request.UrlReferrer.ToString());
        }

    }
}
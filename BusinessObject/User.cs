﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public class User
    {
        public int UserId { get; set; }

        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }

        [Required]
        public string PasswordQuestion { get; set; }
        [Required]
        public string PasswordAnswer { get; set; }

        public bool IsApproved { get; set; }

        [Required]
        public int  RoleId { get; set; }
        [Required]
        public string Language { get; set; }



    }
}

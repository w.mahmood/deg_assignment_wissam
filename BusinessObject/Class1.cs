﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BusinessObject
{
    public class Class1
    {
       public  static string lang = "en-US";
       static public string getTranslated(string name )
        {
            if (lang.Equals("en-US"))
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            }
            else 
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("ar-SY");
            }

            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;

            //ResourceManager rm = new ResourceManager("Resources/Resource", Assembly.GetExecutingAssembly());

            // Retrieve the value of the string resource named "welcome".
            // The resource manager will retrieve the value of the  
            // localized resource using the caller's current culture setting.
            string str = Resources.Resource.ResourceManager.GetString(name);
            return str;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BusinessObject;
using DataAccessLayer;


namespace BusinessLayer
{
   public  class UserBusinessLayer
    {
        public IEnumerable<User> Users
        {
            get
            {
                try
                {
                    UserDA UserDA = new UserDA();
                    return UserDA.AllUsers;

                }
                catch(Exception ex)
                {
                    throw;
                }
               

            }




        }

        public void addUser(User user)
        {
            try
            {
                UserDA UserDA = new UserDA();
                UserDA.addUser(user);

            }
            catch (Exception ex)
            {
                throw;
            }


        }
        public void saveUser(User user)
        {

            try
            {
                UserDA UserDA = new UserDA();
                UserDA.saveUser(user);

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public void deleteUser(int id)
        {

            try
            {
                UserDA UserDA = new UserDA();
                UserDA.deleteUser(id);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
